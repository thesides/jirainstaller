#!/bin/bash
# Init
check_status() {
    status=$?
    if [[ "$status" != "0" ]]; then
        echo "ERROR: $1 ($status)"
        exit 1;
    fi
}
FILE="/tmp/out.$$"
GREP="/bin/grep"
# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
# Install jira for CentOS 8
FWSTATE=$(firewall-cmd --state)
echo "Firewall status: $FWSTATE"
echo "Please select install method:"
echo "1. Confluence"
echo "2. Jira"
echo "3. Confluence + Jira"
echo "4. Exit"
read -p "Selection [3]: " userChoise
userChoise=${userChoise:-3}
echo "choise $userChoise"
if [ "$userChoise" == "4" ]; then
    exit 0;
fi

if [[ "$userChoise" == "1" || "$userChoise" == "3" ]]; then
    read -p "Enter DNS name for Confluence:" cflDNS
    cflDNS=${cflDNS:-cfl.local}
fi

if [[ "$userChoise" == "2" || "$userChoise" == "3" ]]; then
    read -p "Enter DNS name for Jira:" jiraDNS
    jiraDNS=${jiraDNS:-jira.local}
fi


if [ -f /etc/os-release ]; then
    . /etc/os-release
    OS=$NAME
    VERSION=$VERSION_ID
    echo "System: $OS $VERSION"
    if [[ "$OS" == "CentOS Linux" && "$VERSION" == "8" ]];
    then
    	#install tar
    	yum install -y tar
        #Install EPEL repository
        echo "Installing EPEL repository for NGINX"
        dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
        check_status "error compliting adding repository. Nginx can not be installed"
        #NGINX installation
        echo "Installing NGINX"
        yum install -y nginx
        check_status "Error occured while installing Nginx"
        #adding firewall rules
        if [[ "$FWSTATE" == "running" ]]; then
            echo "Applying firewall rules"
            firewall-cmd --permanent --zone=public --add-service=http 
            firewall-cmd --permanent --zone=public --add-service=https
            firewall-cmd --permanent --zone=public --add-port=8080/tcp
            firewall-cmd --permanent --zone=public --add-port=8090/tcp
            firewall-cmd --reload
        fi
        #Enable NGINX
        systemctl enable nginx;
        status=$?
        if [[ "$status" == "0" ]]; then
            echo "Firewall successfuly confirured for nginx"
        fi
        echo "Configuring NGINX for proxying connections"
        mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.backup
        echo "user  nginx;" >> /etc/nginx/nginx.conf
        echo "worker_processes  2;" >> /etc/nginx/nginx.conf
        echo "error_log  /var/log/nginx/error.log warn;" >> /etc/nginx/nginx.conf
        echo "pid        /var/run/nginx.pid;" >> /etc/nginx/nginx.conf
        echo "events {" >> /etc/nginx/nginx.conf
        echo "    worker_connections  1024;" >> /etc/nginx/nginx.conf
        echo "}" >> /etc/nginx/nginx.conf
        echo "http {" >> /etc/nginx/nginx.conf
        echo "    include       /etc/nginx/mime.types;" >> /etc/nginx/nginx.conf
        echo "    default_type  application/octet-stream;" >> /etc/nginx/nginx.conf
        echo "    log_format  main  '\$remote_addr - \$remote_user [\$time_local] \"\$request\" '" >> /etc/nginx/nginx.conf
        echo "                      '\$status \$body_bytes_sent \"\$http_referer\" '" >> /etc/nginx/nginx.conf
        echo "                      '\"\$http_user_agent\" \"\$http_x_forwarded_for\"';" >> /etc/nginx/nginx.conf
        echo "    access_log  /var/log/nginx/access.log  main;" >> /etc/nginx/nginx.conf
        echo "    sendfile        on;" >> /etc/nginx/nginx.conf
        echo "    #tcp_nopush     on;" >> /etc/nginx/nginx.conf
        echo "    keepalive_timeout  65;" >> /etc/nginx/nginx.conf
        echo "    gzip  on;" >> /etc/nginx/nginx.conf
        echo "    gzip_types " >> /etc/nginx/nginx.conf
        echo "        text/plain" >> /etc/nginx/nginx.conf
        echo "        text/css" >> /etc/nginx/nginx.conf
        echo "        text/js" >> /etc/nginx/nginx.conf
        echo "        text/xml" >> /etc/nginx/nginx.conf
        echo "        text/javascript" >> /etc/nginx/nginx.conf
        echo "        application/javascript" >> /etc/nginx/nginx.conf
        echo "        application/x-javascript" >> /etc/nginx/nginx.conf
        echo "        application/json" >> /etc/nginx/nginx.conf
        echo "        application/xml" >> /etc/nginx/nginx.conf
        echo "        application/xml+rss;" >> /etc/nginx/nginx.conf
        echo "    include /etc/nginx/conf.d/*.conf;" >> /etc/nginx/nginx.conf
        echo "}" >> /etc/nginx/nginx.conf
        echo "relocading NGINX config file"
        nginx -s reload
        
        #installing MySQL
        dnf install -y mysql-server
        check_status "Error occured while installing Nginx"
        systemctl start mysqld.service
        state=$(systemctl status mysqld)
        if [[ "$state" == *"active (running)"* ]]; then
            echo "Mysql is active (running)"
        else
            echo "ERROR: Mysql can not be started"
            exit 1
        fi
        systemctl enable mysqld
        check_status "Error occured while installing MySQL"
        
        #Installing CFL
        if [[ "$userChoise" == "1" || "$userChoise" == "3" ]]; then
            inst/atlassian-confluence-6.12.4-x64.bin
            check_status "Error occured while installing CFL"
            cflPath=$(cat /etc/init.d/confluence* | grep cd)
            eval $cflPath && cd ..
            check_status "CFL Install path '$cflPath' is not exists"
            ##Confluence config file
            mv conf/server.xml conf/server.xml.backup
            echo "<Server port=\"8000\" shutdown=\"SHUTDOWN\" debug=\"0\">" >> conf/server.xml
			echo "    <Service name=\"Tomcat-Standalone\">" >> conf/server.xml
			echo "        <Connector port=\"8090\" connectionTimeout=\"20000\" redirectPort=\"8443\"" >> conf/server.xml
			echo "                   maxThreads=\"48\" minSpareThreads=\"10\"" >> conf/server.xml
			echo "                   enableLookups=\"false\" acceptCount=\"10\" debug=\"0\" URIEncoding=\"UTF-8\"" >> conf/server.xml
			echo "                   protocol=\"org.apache.coyote.http11.Http11NioProtocol\"" >> conf/server.xml
			echo "                   scheme=\"http\" proxyName=\"cfl.local\" proxyPort=\"80\"/>" >> conf/server.xml
			echo "            <Engine name=\"Standalone\" defaultHost=\"localhost\" debug=\"0\">" >> conf/server.xml
			echo "            <Host name=\"localhost\" debug=\"0\" appBase=\"webapps\" unpackWARs=\"true\" autoDeploy=\"false\" startStopThreads=\"4\">" >> conf/server.xml
			echo "                <Context path=\"\" docBase=\"../confluence\" debug=\"0\" reloadable=\"false\" useHttpOnly=\"true\">" >> conf/server.xml
			echo "                    <!-- Logging configuration for Confluence is specified in confluence/WEB-INF/classes/log4j.properties -->" >> conf/server.xml
			echo "                    <Manager pathname=\"\"/>" >> conf/server.xml
			echo "                    <Valve className=\"org.apache.catalina.valves.StuckThreadDetectionValve\" threshold=\"60\"/>" >> conf/server.xml
			echo "                </Context>" >> conf/server.xml
			echo "" >> conf/server.xml
			echo "                <Context path=\"\${confluence.context.path}/synchrony-proxy\" docBase=\"../synchrony-proxy\" debug=\"0\"" >> conf/server.xml
			echo "                         reloadable=\"false\" useHttpOnly=\"true\">" >> conf/server.xml
			echo "                    <Valve className=\"org.apache.catalina.valves.StuckThreadDetectionValve\" threshold=\"60\"/>" >> conf/server.xml
			echo "                </Context>" >> conf/server.xml
			echo "            </Host>" >> conf/server.xml
			echo "        </Engine>" >> conf/server.xml
			echo "    </Service>" >> conf/server.xml
			echo "</Server>" >> conf/server.xml
            
            ##End config file
            echo "checking hosts file" 
            testDns=$(cat /etc/hosts | grep $cflDNS)
            if [[ "$testDns" == "" ]]; then
                hostIp=$(hostname -i | grep -oP '(\d+\.\d+\.\d+\.\d+)')
                echo "Applying host: $cflDNS to $hostIp"
                echo "$hostIp   $cflDNS" >> /etc/hosts
            fi
            
            ##Cconfiguring nginx
            mv /etc/nginx/conf.d/confluence.conf /etc/nginx/conf.d/confluence.conf.backup
            echo "server {" >> /etc/nginx/conf.d/confluence.conf
            echo "    listen cfl.local:80;" >> /etc/nginx/conf.d/confluence.conf
            echo "    server_name cfl.local;" >> /etc/nginx/conf.d/confluence.conf
            echo "    location /cfl {" >> /etc/nginx/conf.d/confluence.conf
            echo "        proxy_set_header X-Forwarded-Host \$host;" >> /etc/nginx/conf.d/confluence.conf
            echo "        proxy_set_header X-Forwarded-Server \$host;" >> /etc/nginx/conf.d/confluence.conf
            echo "        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for; " >> /etc/nginx/conf.d/confluence.conf
            echo "        proxy_pass http://cfl.local:8090/cfl;" >> /etc/nginx/conf.d/confluence.conf
            echo "        client_max_body_size 10M;" >> /etc/nginx/conf.d/confluence.conf
            echo "    }" >> /etc/nginx/conf.d/confluence.conf
            echo "}" >> /etc/nginx/conf.d/confluence.conf
            ##End of config
            systemctl restart nginx
            /etc/init.d/confluence* stop
            /etc/init.d/confluence* start
        fi
        
        #Installinf Jira
        if [[ "$userChoise" == "2" || "$userChoise" == "3" ]]; then
            inst/atlassian-jira-software-7.12.3-x64.bin
            check_status "Error occured while installing CFL"
            jiraPath=$(cat /etc/init.d/jira* | grep cd)
            eval $jiraPath && cd ..
            check_status "CFL Install path '$jiraPath' is not exists"
            mv conf/server.xml conf/server.xml.backup
            echo "Creating config file for Jira"
            echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>" >> conf/server.xml
            echo "        <Server port=\"8005\" shutdown=\"SHUTDOWN\">" >> conf/server.xml
            echo "            <Listener className=\"org.apache.catalina.startup.VersionLoggerListener\"/>" >> conf/server.xml
            echo "            <Listener className=\"org.apache.catalina.core.AprLifecycleListener\" SSLEngine=\"on\"/>" >> conf/server.xml
            echo "            <Listener className=\"org.apache.catalina.core.JreMemoryLeakPreventionListener\"/>" >> conf/server.xml
            echo "            <Listener className=\"org.apache.catalina.mbeans.GlobalResourcesLifecycleListener\"/>" >> conf/server.xml
            echo "            <Listener className=\"org.apache.catalina.core.ThreadLocalLeakPreventionListener\"/>" >> conf/server.xml
            echo "            <Service name=\"Catalina\">" >> conf/server.xml
            echo "                <Connector port=\"8080\" relaxedPathChars=\"[]|\" relaxedQueryChars=\"[]|{}^&#x5c;&#x60;&quot;&lt;&gt;\"" >> conf/server.xml
            echo "                       maxThreads=\"150\" minSpareThreads=\"25\" connectionTimeout=\"20000\" enableLookups=\"false\"" >> conf/server.xml
            echo "                        maxHttpHeaderSize=\"8192\" protocol=\"HTTP/1.1\" useBodyEncodingForURI=\"true\" redirectPort=\"8443\"" >> conf/server.xml
            echo "                        acceptCount=\"100\" disableUploadTimeout=\"true\" bindOnInit=\"false\" scheme=\"http\"" >> conf/server.xml
            echo "                        proxyName=\"jira.local\" proxyPort=\"80\"/>" >> conf/server.xml
            echo "                <Engine name=\"Catalina\" defaultHost=\"localhost\">" >> conf/server.xml
            echo "                    <Host name=\"localhost\" appBase=\"webapps\" unpackWARs=\"true\" autoDeploy=\"true\">" >> conf/server.xml
            echo "                        <Context path=\"jira\" docBase=\"\${catalina.home}/atlassian-jira\" reloadable=\"false\" useHttpOnly=\"true\">" >> conf/server.xml
            echo "                            <Resource name=\"UserTransaction\" auth=\"Container\" type=\"javax.transaction.UserTransaction\"" >> conf/server.xml
            echo "                                   factory=\"org.objectweb.jotm.UserTransactionFactory\" jotm.timeout=\"60\"/>" >> conf/server.xml
            echo "                            <Manager pathname=\"\"/>" >> conf/server.xml
            echo "                            <JarScanner scanManifest=\"false\"/>" >> conf/server.xml
            echo "                       </Context>" >> conf/server.xml
            echo "                    </Host>" >> conf/server.xml
            echo "                    <Valve className=\"org.apache.catalina.valves.AccessLogValve\"" >> conf/server.xml
            echo "                        pattern=\"%a %{jira.request.id}r %{jira.request.username}r %t &quot;%m %U%q %H&quot; %s %b %D &quot;%{Referer}i&quot; &quot;%{User-Agent}i&quot; &quot;%{jira.request.assession.id}r&quot;\"/>" >> conf/server.xml
            echo "                </Engine>" >> conf/server.xml
            echo "            </Service>" >> conf/server.xml
            echo "        </Server>" >> conf/server.xml
            
            echo "checking hosts file" 
            testDns=$(cat /etc/hosts | grep $jiraDNS)
            if [[ "$testDns" == "" ]]; then
                hostIp=$(hostname -i | grep -oP '(\d+\.\d+\.\d+\.\d+)')
                echo "Applying host: $jiraDNS to $hostIp"
                echo "$hostIp   $jiraDNS" >> /etc/hosts
            fi
            
            mv /etc/nginx/conf.d/jira.conf /etc/nginx/conf.d/jira.conf.backup
            echo "Creating NGINX config for Jira"
            echo "server {" >> /etc/nginx/conf.d/jira.conf
            echo "        listen jira.local:80;" >> /etc/nginx/conf.d/jira.conf
            echo "        server_name jira.local;" >> /etc/nginx/conf.d/jira.conf
            echo "        location /jira {" >> /etc/nginx/conf.d/jira.conf
            echo "            proxy_set_header X-Forwarded-Host \$host;" >> /etc/nginx/conf.d/jira.conf
            echo "            proxy_set_header X-Forwarded-Server \$host;" >> /etc/nginx/conf.d/jira.conf
            echo "            proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for; " >> /etc/nginx/conf.d/jira.conf
            echo "            proxy_pass http://jira.local:8080/jira;" >> /etc/nginx/conf.d/jira.conf
            echo "            client_max_body_size 10M;" >> /etc/nginx/conf.d/jira.conf
            echo "        }" >> /etc/nginx/conf.d/jira.conf
            echo "    }" >> /etc/nginx/conf.d/jira.conf
            echo "Reloading NGINX"
            systemctl restart nginx
            echo "Stopping jira"
            /etc/init.d/jira* stop
            echo "Starting jira"
            /etc/init.d/jira* start
        fi
    fi
fi
echo "Instalation script completed"
